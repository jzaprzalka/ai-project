# %% [markdown]
# # Importing distance matrix from Google Maps API
# %% [markdown]
# ## Imports
# Import all the required libraries and prepare environment
from __future__ import division

import json
import os
from datetime import datetime
from urllib.request import urlopen

import numpy as np
import requests

JSON_FILE_STYLE = './json/distance_matrix_fix_{}{}.json'
JSON_ADDRESS_STYLE = './json/json_{}.json'
API_KEY = 'yzJqR18v9ChH1uB5gNOTHYdvbpA1h'

data = {}
data['Online'] = True

# %% [markdown]
# ## Functions to use 
# - `send_request` builds sends request for the given addresses
# - `build_distance_matrix` builds a distance matrix based on the response
def send_request(origin_addresses, dest_addresses, file_name='./json/distance_matrixv2.json', force=False):
    """
    Build and send request for the given origin addresses.
    """
    if os.path.isfile(file_name) and not force:
        with open(file_name, 'r') as f:
            response = json.load(f)
    else:
        o_addresses = '|'.join(origin_addresses)
        d_addresses = '|'.join(dest_addresses)
        request = f'https://api.distancematrix.ai/maps/api/distancematrix/json?origins={o_addresses}&destinations={d_addresses}&key={API_KEY}'
        jsonResult = urlopen(request).read()
        # print(jsonResult)
        if not os.path.isdir(os.path.dirname(file_name)):
            os.makedirs(os.path.dirname(file_name))
        with open(file_name, 'w') as f:
            response = json.loads(jsonResult)
            json.dump(response, f)
    return response

def get_location(address, filename_style=JSON_ADDRESS_STYLE, force=False):
    """
    Get location based on the address. 
    returns `(loc_x, loc_y)`
    """
    file_name = filename_style.format(address)
    if os.path.isfile(file_name) and not force:
        with open(file_name, 'r') as f:
            response = json.load(f)
    else:
        request = f'https://api.distancematrix.ai/maps/api/geocode/json?address={address}&key={API_KEY}'
        jsonResult = urlopen(request).read()
        if not os.path.isdir(os.path.dirname(file_name)):
            os.makedirs(os.path.dirname(file_name))
        with open(file_name, 'w') as f:
            response = json.loads(jsonResult)
            json.dump(response, f)
    loc = response['result'][0]['geometry']['location']
    print(loc)
    loc_xy = (loc['lat'], loc['lng'])
    return loc_xy

def build_distance_matrix(response):
    distance_matrix = []
    for row in response['rows']:
        row_list = [row['elements'][j]['distance']['value']
                    for j in range(len(row['elements']))]
        distance_matrix.append(row_list)
    return np.matrix(distance_matrix)

def build_time_matrix(response):
    time_matrix = []
    for row in response['rows']:
        row_list = [row['elements'][j]['duration']['value']
                    for j in range(len(row['elements']))]
        time_matrix.append(row_list)
    return np.matrix(time_matrix)

def get_addresses(response):
    location = [response['destination_addresses'][j]
                for j in range(len(response['destination_addresses']))]
    return location

# %% [markdown]
# ## Create the data - addresses
request_addresses = [
    # Section 0
    'Lipowa+18,+15-427+Bialystok',
    'Antoniukowska+56,+15-845+Bialystok',
    'Wasilkowska+1,+15-145+Bialystok',
    'Wysoki+Stoczek+54,+15-754+Bialystok',
    'Wiejska+65,+15-351+Bialystok',
    'Piekna+2,+15-369+Bialystok',
    'Aleja+Jana+Pawla+II+51,+15-704+Bialystok',
    'Wladyslawa+Broniewskiego+14,+15-959+Bialystok',
    'Wesola+16,+15-307+Bialystok',
    'Wiewiorcza+64,+15-532+Bialystok',
    # Section 1
    'Aleja+Tysiaclecia+Panstwa+Polskiego+14,+15-000+Bialystok',
    'Swobodna+54F,+15-751+Bialystok',
    'Produkcyjna+102,+15-680+Bialystok',
    'Ryska+1,+15-008+Bialystok',
    'Towarowa+1,+15-007+Bialystok',
    'Generala+Wladyslawa+Andersa+42,+15-113+Bialystok',
    'Legionowa+3,+15-267+Bialystok',
    'Warszawska+8,+15-063+Bialystok',
    '42+Pulku+Piechoty+34,+15-147+Bialystok',
    'Antoniukowska+56,+15-845+Bialystok',
    # Section 2
    'Hugona+Kollataja+22,+15-774+Bialystok',
    'Klepacka+2,+15-634+Bialystok',
    'Naftowa+3,+15-541+Bialystok',
    'zurawia+14,+15-540+Bialystok',
    'Baranowicka+144,+15-517+Bialystok'
]

#+%%+[markdown]
# ## Create distance matrix
# - Distance Matrix API only accepts 10x10 matrices.
# - Send requests, returning `max_elements` per request.
# - Build and load the data
#   - Time matrix
#   - Distance matrix

# Distance Matrix API only accepts 10x10 matrices.
max_elements = 10
num_addresses = len(request_addresses)  

iters = np.arange(int(np.ceil(num_addresses / max_elements)))

distance_matrix = None
time_matrix = None

start_end = lambda i: {
    'start': i * max_elements, 
    'end': (i + 1) * max_elements if (i + 1) * max_elements <= num_addresses else None
}

# Send requests, returning max_elements per request and build the matrices.
for i in iters:
    distance_row = None
    time_row = None
    o = start_end(i)
    origin_addresses = request_addresses[o['start']: o['end']]
    for j in iters:
        d = start_end(j)
        dest_addresses = request_addresses[d['start']: d['end']]
        file = JSON_FILE_STYLE.format(i, j)
        print(f'Reading/Getting file [{file}]')
        response = send_request(
            origin_addresses, 
            dest_addresses,
            file_name=file
        )
        # Connect distance rows Column wise
        if distance_row is None:
            distance_row = build_distance_matrix(response)
        else: 
            distance_row = np.hstack((distance_row, build_distance_matrix(response)))
        # Connect time rows Column wise
        if time_row is None:
            time_row = build_time_matrix(response)
        else: 
            time_row = np.hstack((time_row, build_time_matrix(response)))
    # Connect distance rows Row wise
    if distance_matrix is None:
        distance_matrix = distance_row
    else: 
        distance_matrix = np.vstack((distance_matrix, distance_row))
    # Connect time rows Row wise
    if time_matrix is None:
        time_matrix = time_row
    else: 
        time_matrix = np.vstack((time_matrix, time_row))

# %% [markdown]
# ### Assign and show the distance matrix
data['distance_matrix'] = distance_matrix.tolist()
print(f'Distance matrix shape: {distance_matrix.shape}, Max: {time_matrix.max()}')

# %% [markdown]
# ### Assign and show the time matrix
time_matrix *= (1/60)
data['time_matrix'] = time_matrix.astype(int).tolist()
print(f'Time matrix shape: {time_matrix.shape}, Max: {time_matrix.max()}')

# %% [markdown]
# ## Fill locations
# Fill the locations for the Addresses and normalize them
locations = []
for a in request_addresses:
    locations.append(get_location(a))
locations

# %% [markdown]
# ## Normalize locations
# Normalize the locations
locations = np.array(locations)
locations_normalized = (locations - locations.min(axis=0))
locations_normalized /= locations_normalized.max(axis=0)
locations_normalized *= 500
locations_normalized

# %% [markdown]
# Locations for addresses
data['locations'] = locations_normalized.tolist()
