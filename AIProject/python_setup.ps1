Set-ExecutionPolicy Unrestricted -Scope Process
py -m pip install --upgrade pip virtualenv
py -m venv env
./env/Scripts/activate.ps1
pip install --upgrade -r ./requirements.txt