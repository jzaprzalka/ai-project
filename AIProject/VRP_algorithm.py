# %% [markdown]
# # Vehicle Routing Problem
# VRP with:
# - [x] Pickups and Deliveries Constraints 
# - [x] Capacity Constraints
# - [x] Time Window Constraints
# - [x] Rescource Constraints
# - [x] Penalties And Dropping Visits
# %% [markdown]
# ## Imports
# Import all the required libraries
import numpy as np
from matplotlib import pyplot as plt
from ortools.constraint_solver import pywrapcp, routing_enums_pb2

FROM_GOOGLE_MAPS = True

if FROM_GOOGLE_MAPS: 
    from distance_matrix_maps import data
else:
    from distance_matrix_manual import data

# %% [markdown]
# ## Prepare the data
# Define model values
locations = np.array(data['locations'])
data['num_vehicles'] = 4
data['depot'] = 0
data['vehicle_load_time'] = 2
data['vehicle_unload_time'] = 3
data['depot_capacity'] = 2

# %% [markdown]
# Add pickup-delivery request
data['pickups_deliveries'] = [
    [1, 6],
    [2, 10],
    [4, 3],
    [5, 9],
    [7, 8],
    [15, 11],
    [13, 12],
    [16, 14],
]
data['pickups_deliveries']

# %% [markdown]
# Add Time Window for pickups.
# data['time_windows'] =  [(0, 300)] * 25
data['time_windows'] = [
    (0, 60),     # depot
    (0, 90),    # 1
    (3, 6),   # 2
    (6, 180),   # 3
    (0, 30),     # 4
    (0, 180),     # 5
    (20, 50),    # 6
    (15, 100),     # 7
    (14, 110),    # 8
    (3, 150),     # 9
    (1, 100),   # 10
    (16, 310),   # 11
    (0, 1),     # 12
    (2, 70),    # 13
    (1, 30),     # 14
    (2, 60),   # 15
    (3, 50),   # 16
    (5, 90),    # 17
    (8, 120),   # 18
    (7, 80),   # 19
    (6, 70),   # 20
    (12, 140),     # 21
    (10, 120),    # 22
    (6, 70),     # 23
    (2, 60)    # 24
]
data['time_windows']

# %% [markdown]
# Add capacity request
MIN_LOAD_SIZE = 3
MAX_LOAD_SIZE = 6
data['vehicle_capacities'] = [20] * data['num_vehicles']
data['demands'] = [0] * len(data['locations'])
for (i, j) in data['pickups_deliveries']:
    demand = np.random.randint(MIN_LOAD_SIZE, MAX_LOAD_SIZE + 1)
    data['demands'][i] += demand
    data['demands'][j] += -demand # + np.random.randint(MIN_LOAD_SIZE, MAX_LOAD_SIZE + 1)
data['demands']


# %% [markdown]
# Create the routing index manager.
manager = pywrapcp.RoutingIndexManager(
    len(data['time_matrix']),
    data['num_vehicles'], 
    data['depot']
)

# %% [markdown]
# ## Visualize the problem
fig, ax = plt.subplots(figsize=(15, 12))
ax.scatter(*locations.T)
marker_size = 20
for i, loc in enumerate(locations):
    ax.annotate(
        s=f"{i}", 
        xy=loc, 
        xycoords='data',
        va='center',
        ha='center',
        size=marker_size,
        bbox=dict(boxstyle='circle', fc='w', ec='b'))
    ax.annotate(
        s=f"[{data['demands'][i]}]", 
        xy=loc, 
        xytext=(0, marker_size + 5),
        xycoords='data',
        textcoords='offset points',
        va='center',
        ha='center',
        size=marker_size / 1.2,
        color='red')
    ax.annotate(
        s=f"{data['time_windows'][i]}", 
        xy=loc, 
        xytext=(0, -(marker_size + 5)),
        xycoords='data',
        textcoords='offset points',
        va='center',
        ha='center',
        size=marker_size / 1.2,
        color='green')
for pd_request in data['pickups_deliveries']:
    arrow = np.array([
        locations[pd_request[0]], 
        locations[pd_request[1]] - locations[pd_request[0]]
    ])
    arrow_length = np.linalg.norm(arrow[1])
    arrow_scale = (arrow_length - marker_size) / arrow_length
    ax.arrow(
        *arrow[0],
        *(arrow[1] * arrow_scale),
        color='red',
        width=4,
        head_width=12,
        head_length=20,
        overhang=.3,
        length_includes_head=True,
    )
ax.grid(which='both')
ax.set_title(
'''Points locations on the grid with pickup-delivery constraints,
capacity constraints marked in red, rescourse constraints marked in green''', 
    fontsize=20)
plt.show()

# %% [markdown]
# ## Create Routing Model
routing = pywrapcp.RoutingModel(manager)

# %% [markdown]
# ### Create and register a distance transit callback.
def distance_callback(from_index, to_index):
    """
    Returns the distance between the two nodes.
    """
    # Convert from routing variable Index to distance matrix NodeIndex.
    from_node = manager.IndexToNode(from_index)
    to_node = manager.IndexToNode(to_index)
    return data['distance_matrix'][from_node][to_node]

distance_callback_index = routing.RegisterTransitCallback(distance_callback)

# %% [markdown]
# ### Create and register a time transit callback.
def time_callback(from_index, to_index):
    """
    Returns the time between the two nodes.
    """
    # Convert from routing variable Index to distance matrix NodeIndex.
    from_node = manager.IndexToNode(from_index)
    to_node = manager.IndexToNode(to_index)
    return data['time_matrix'][from_node][to_node]

time_callback_index = routing.RegisterTransitCallback(time_callback)

# %% [markdown]
# ### Define cost of each arc as time between two nodes.
routing.SetArcCostEvaluatorOfAllVehicles(time_callback_index)

# %% [markdown]
# ### Create and register a capacity demand callback.
def demand_callback(from_index):
    """
    Returns the demand of the node.
    """
    # Convert from routing variable Index to distance matrix NodeIndex.
    from_node = manager.IndexToNode(from_index)
    return data['demands'][from_node]

demand_callback_index = routing.RegisterUnaryTransitCallback(demand_callback)

# %% [markdown]
# ### Add Distance constraint.
dimension_name = 'Distance'
routing.AddDimension(
    distance_callback_index,
    0,      # no slack
    100000,   # vehicle maximum travel distance
    True,   # start cumul to zero
    dimension_name)
distance_dimension = routing.GetDimensionOrDie(dimension_name)
distance_dimension.SetGlobalSpanCostCoefficient(100)

# %% [markdown]
# ### Add Demand constraint.
dimension_name = 'Capacity'
routing.AddDimensionWithVehicleCapacity(
    demand_callback_index,
    0,                          # no slack
    data['vehicle_capacities'], # vehicle maximum capacities
    True,                       # start cumul to zero
    dimension_name)
demand_dimension = routing.GetDimensionOrDie(dimension_name)

# %% [markdown]
# ### Add Time Window constraint.
dimension_name = 'Time'
routing.AddDimension(
    time_callback_index,
    3000,     # allow waiting time
    3000,     # maximum time per vehicle
    False,  # Don't force start cumul to zero.
    dimension_name)
time_dimension = routing.GetDimensionOrDie(dimension_name)

# %% [markdown]
# Add time window constraints for each location except depot.
for location_idx, time_window in enumerate(data['time_windows']):
    if location_idx == data['depot']:
        continue
    index = manager.NodeToIndex(location_idx)
    time_dimension.CumulVar(index).SetRange(time_window[0], time_window[1])
    print(index)

# %% [markdown]
# Add time window constraints for each vehicle start node.
for vehicle_id in range(data['num_vehicles']):
    index = routing.Start(vehicle_id)
    time_dimension.CumulVar(index).SetRange(*(data['time_windows'][data['depot']]))

# %% [markdown]
# Add resource constraint at the depot.
solver = routing.solver()
intervals = []
for i in range(data['num_vehicles']):
    # Add time windows at start of routes
    intervals.append(
        solver.FixedDurationIntervalVar(
            time_dimension.CumulVar(routing.Start(i)),
            data['vehicle_load_time'], 'depot_interval'))
    # Add time windows at end of routes.
    intervals.append(
        solver.FixedDurationIntervalVar(
            time_dimension.CumulVar(routing.End(i)),
            data['vehicle_unload_time'], 'depot_interval'))

# %% [markdown]
# Add depot usage to depot.
depot_usage = [1] * len(intervals)
solver.Add(
    solver.Cumulative(intervals, depot_usage, data['depot_capacity'], 'depot')
)

# %% [markdown]
# Instantiate route start and end times to produce feasible times.                      
for i in range(data['num_vehicles']):
    routing.AddVariableMinimizedByFinalizer(
        time_dimension.CumulVar(routing.Start(i))
    )
    routing.AddVariableMinimizedByFinalizer(
        time_dimension.CumulVar(routing.End(i))
    )

# %% [markdown]
### Define Transportation Requests for pickups and deliveries
for request in data['pickups_deliveries']:
    pickup_index = manager.NodeToIndex(request[0])
    delivery_index = manager.NodeToIndex(request[1])
    routing.AddPickupAndDelivery(pickup_index, delivery_index)
    routing.solver().Add(
        routing.VehicleVar(pickup_index) == routing.VehicleVar(delivery_index)
    )
    routing.solver().Add(
        distance_dimension.CumulVar(pickup_index) <= distance_dimension.CumulVar(delivery_index)
    )

# %% [markdown]
# ### Allow to drop nodes.
penalty = np.sum(data['distance_matrix']).item()
for node in range(1, len(data['distance_matrix'])):
    routing.AddDisjunction([manager.NodeToIndex(node)], penalty)

# %% [markdown]
# ## Set search params
# Set the search parameters according to the problem and the time limit for the solution. 
search_parameters = pywrapcp.DefaultRoutingSearchParameters()
search_parameters.first_solution_strategy = (
    routing_enums_pb2.FirstSolutionStrategy.LOCAL_CHEAPEST_INSERTION
)
search_parameters.local_search_metaheuristic = (
    routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH
)
search_parameters.time_limit.seconds = 30
search_parameters.log_search = True
search_parameters

# %% [markdown]
# ## Add monitor callback
# Add a callback to show each solution found by the solver
solutions = []
class SolutionCallback:

    def __init__(self, model):
        self.model = model
        self.previous = None
    
    def __call__(self):
        model_cost = self.model.CostVar().Max()
        dropped_nodes = model_cost // penalty
        if not self.previous or self.previous > model_cost:
            print(f'\rFound better solution with cost {model_cost} ({dropped_nodes} dropped nodes)')
        else: print('.', end='')
        self.previous = model_cost
        solutions.append(model_cost)

solution_callback = SolutionCallback(routing)
routing.AddAtSolutionCallback(
    solution_callback
)
# %% [markdown]
# ## Solve the problem.
# The solution takes no shorter than the `time_limit` set. 
# The solver doesn't stop until the `time_limit` is reached.
# This is due to the first solution strategy set to `LOCAL_CHEAPEST_INSERTION`
# which keeps inserting the nodes at nearest positions and only works with is 
# the fastest solver for VRP with disjunction (see __Dropping nodes__ above).
solution = routing.SolveWithParameters(search_parameters)

# %% [markdown]
# ## Plot solutions cost
# Plot the values for solutions in time
plt.plot(solutions)
plt.title('Solutions cost over time')
plt.xlabel('Time')
plt.ylabel('Cost')
plt.show()

# %% [markdown]
# ## Print the solution
# - Display routes
# - Display dropped nodes
dropped_ids = []
if solution:
    # Display routes
    total_distance = 0
    global_max_load = 0
    time_dimension = routing.GetDimensionOrDie('Time')
    total_time = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        print(f'Route for vehicle {vehicle_id}:')
        route_distance = 0
        max_route_load = 0
        route_load = 0
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            route_load += data['demands'][node_index]
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            time_var = time_dimension.CumulVar(index)
            print(f' {node_index} Load({route_load}) --Time({solution.Min(time_var)}, {solution.Max(time_var)})-->')
            route_distance += data['distance_matrix'][node_index][manager.IndexToNode(index)]
            max_route_load = max(route_load, max_route_load)
        node_index = manager.IndexToNode(index)
        print(f'{node_index} Load({route_load})\n')
        print(f'Distance of the route: {route_distance}m')
        print(f'Time of the route: {solution.Min(time_var)}min')
        total_distance += route_distance
        total_time += solution.Min(time_var)
        print(f'Max load of the route: {max_route_load}\n-------------')
        global_max_load = max(global_max_load, max_route_load)
    print(f'Total distance of all routes: {total_distance}m')
    print(f'Total time of all routes: {total_time}min')
    print(f'Maximum load for all vehicles: {global_max_load}')
    print('=====================')
    # Display dropped nodes.
    for node in range(routing.Size()):
        if routing.IsStart(node) or routing.IsEnd(node):
            continue
        if solution.Value(routing.NextVar(node)) == node:
            dropped_ids.append(manager.IndexToNode(node))
    print(f'Dropped nodes: {dropped_ids if dropped_ids else None}')
else:
    print('No solution found')

# %% [markdown]
# ## Visualize the solution
fig, ax = plt.subplots(figsize=(15, 12))
ax.scatter(*locations.T)
marker_size = 20
for i, loc in enumerate(locations):
    ax.annotate(
        s=f"{i}", 
        xy=loc, 
        xycoords='data',
        va='center',
        ha='center',
        size=marker_size,
        bbox=dict(
            boxstyle='circle', 
            fc='w', 
            ec='blue' if i not in dropped_ids else 'red'
        )
    )
if solution:
    colors = plt.cm.rainbow(np.linspace(0, 1, data['num_vehicles']))
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        while not routing.IsEnd(index):
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            arrow = np.array([
                locations[manager.IndexToNode(previous_index)], 
                locations[manager.IndexToNode(index)] - locations[manager.IndexToNode(previous_index)]
            ])
            arrow_length = np.linalg.norm(arrow[1])
            arrow_scale = (arrow_length - marker_size) / arrow_length
            ax.arrow(
                *arrow[0],
                *(arrow[1] * arrow_scale),
                color=colors[vehicle_id],
                alpha=.6,
                label=f'Vehicle {vehicle_id}',
                width=4,
                head_width=12,
                head_length=20,
                overhang=.3,
                length_includes_head=True,
            )
    ax.grid(which='both')
    ax.set_title(f'Solution to VRP with {data["num_vehicles"]} vehicles\nincluding all listed constraints', fontsize=20)
    plt.show()
